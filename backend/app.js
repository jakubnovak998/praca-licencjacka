const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require('mongoose');

const usersRoutes = require("./routes/user");
const dietGoalsRoutes = require("./routes/dietGoal");
const productRoutes = require("./routes/product");
const dishRoutes = require("./routes/dish");
const planRoutes = require("./routes/plan");

const app = express();

mongoose.connect('mongodb://localhost:27017/DietApp', {useNewUrlParser: true})
  .then(() => {
    console.log('Connected to db');
  })
  .catch( () => {
    console.log('failed connection');
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, OPTIONS"
  );
  next();
});

app.get("/api/test", (req, res, next) => {
  const test = "Test done successfully"
  res.status(200).json({
    message: "Success!",
    test: test
  });
});

app.use("/api/user", usersRoutes);
app.use("/api/diet-goal", dietGoalsRoutes);
app.use("/api/product", productRoutes);
app.use("/api/dish", dishRoutes);
app.use("/api/plan", planRoutes);

module.exports = app;
