const mongoose = require("mongoose");

const dietGoalSchema = mongoose.Schema({
    userId: { type: String, required: true },
    value: { type: Number, required: true },
    unit: { type: String, required: true },
    startDate: {type: String, required: true}
});

module.exports = mongoose.model("dietGoal", dietGoalSchema);