const mongoose = require("mongoose");

const dishPlanSchema = mongoose.Schema({
    userId: { type: String, required: true },
    productId: { type: String, required: true },
    planId: { type: String, required: true },
    quantity: { type: Number, required: true },
});

module.exports = mongoose.model("dishPlan", dishPlanSchema);