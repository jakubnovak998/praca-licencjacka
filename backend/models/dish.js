const mongoose = require("mongoose");
const Product = require("./product");

const dishSchema = mongoose.Schema({
    userId: { type: String, required: true },
    name: { type: String, required: true },
    kcalAmount: { type: Number, required: true },
    proteinAmount: {type: Number, required: true},
    carbohydratesAmount: {type: Number, required: true},
});

module.exports = mongoose.model("dish", dishSchema);