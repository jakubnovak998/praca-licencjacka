const mongoose = require("mongoose");

const ingredientSchema = mongoose.Schema({
    userId: { type: String, required: true },
    productId: { type: String, required: true },
    quantity: { type: Number, required: true },
    dishId: { type: String, required: true },
});

module.exports = mongoose.model("ingredient", ingredientSchema);