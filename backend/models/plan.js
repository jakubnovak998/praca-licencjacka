const mongoose = require("mongoose");

const planSchema = mongoose.Schema({
    userId: { type: String, required: true },
    date: { type: String, required: true },
    kcalAmount: { type: Number, required: true },
    proteinAmount: {type: Number, required: true},
    carbohydratesAmount: {type: Number, required: true}
});

module.exports = mongoose.model("plan", planSchema);