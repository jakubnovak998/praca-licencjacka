const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
    userId: { type: String, required: true },
    name: { type: String, required: true },
    kcalAmount: { type: Number, required: true },
    proteinAmount: { type: Number, required: true },
    carbohydratesAmount: { type: Number, required: true },
});

module.exports = mongoose.model("Product", productSchema);