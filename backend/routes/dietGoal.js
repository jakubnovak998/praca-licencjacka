const express = require("express");
const DietGoal = require("../models/dietGoal");
const checkAuth = require("../middleware/check-auth");

const router = express.Router();

router.post("", checkAuth, (req, res, next) => {
    const dietGoal = new DietGoal({
        startDate: req.body.startDate,
        userId: req.userData.userId,
        unit: req.body.unit,
        value: req.body.value
    })
    dietGoal.save()
        .then(result => {
            res.status(201).json({
                message: "Diet goal created",
                result: result
            })
        }).catch(err => {
            res.status(500).json({
                error: err
            });
        });
})

router.put("", checkAuth, async (req, res, next) => {
    const dietGoal = await DietGoal.findOne({ userId: req.userData.userId });
    dietGoal.startDate = req.body.startDate;
    dietGoal.userId = req.userData.userId;
    dietGoal.unit = req.body.unit;
    dietGoal.value = req.body.value;

    dietGoal.save()
        .then(result => {
            res.status(200).json({
                message: "Diet goal updated",
                result: result
            })
        }).catch(err => {
            res.status(500).json({
                error: err
            });
        });
})

router.get("", checkAuth, (req, res, next) => {
    DietGoal.find({ userId: req.userData.userId }, (err, dietGoal) => {
        if (err) console.error(err);

        res.status(200).json({
            dietGoal: dietGoal,
        })
    })
})

module.exports = router;
