const express = require("express");
const checkAuth = require("../middleware/check-auth");
const Dish = require("../models/dish");
const Ingredient = require("../models/ingredient");
const router = express.Router();

router.post("", checkAuth, (req, res, next) => {
    console.log(req.userData);
    const dish = new Dish({
        userId: req.userData.userId,
        name: req.body.name,
        kcalAmount: req.body.kcalAmount,
        proteinAmount: req.body.proteinAmount,
        carbohydratesAmount: req.body.carbohydratesAmount
    })

    const ingredients = req.body.ingredients;

    dish.save()
        .then(result => {
            for (let i = 0; i < ingredients.length; i++) {
                const ingredient = new Ingredient({
                    userId: req.userData.userId,
                    productId: ingredients[i].productId,
                    quantity: ingredients[i].quantity,
                    dishId: result._id
                });
                ingredient.save()
            }
            res.status(201).json({
                message: "Dish created",
                result: result
            })
        }).catch(err => {
            res.status(500).json({
                error: err
            });
        });
})

router.get("", checkAuth, (req, res, next) => {
    Dish.find({ userId: req.userData.userId }, (err, dishes) => {
        console.log(dishes);

        if (err) console.error(err);

        res.status(200).json({
            dishes: dishes
        })
    })
})

module.exports = router;
