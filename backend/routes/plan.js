const express = require("express");
const checkAuth = require("../middleware/check-auth");
const Plan = require("../models/plan");
const DishPlan = require("../models/dish-plan");
const Dish = require("../models/dish");
const Product = require("../models/product");

const router = express.Router();

router.post("", checkAuth, (req, res, next) => {
    /// logika update'u
    const plan = new Plan({
        userId: req.userData.userId,
        date: req.body.date,
        kcalAmount: req.body.kcalAmount,
        proteinAmount: req.body.proteinAmount,
        carbohydratesAmount: req.body.carbohydratesAmount
    })

    const dishes = req.body.dishes;

    plan.save()
        .then(result => {
            console.log(result);
            for (let i = 0; i < dishes.length; i++) {
                console.log(dishes[i]);
                console.log(dishes[i].product._id);
                console.log(dishes[i].quantity);
                console.log(result._id);
                console.log(req.userData.userId);

                const dishPlan = new DishPlan({
                    userId: req.userData.userId,
                    productId: dishes[i].product._id,
                    quantity: dishes[i].quantity,
                    planId: result._id
                });
               

                dishPlan.save()
            }
            res.status(201).json({
                message: "Plan created",
                result: result
            })
        }).catch (err => {
            res.status(500).json({
                error: err
            });
        });
})

router.get("", checkAuth, async(req, res) => {
    var date = req.query.date;

    Plan.find({userId: req.userData.userId, date: date}, async(err, plan) => {
        if (err) {
            console.error(err);
            res.status(500).json({
                error: err
            })
            return;
        }

        if (!plan[0]) {
            res.status(200).json({
                plan: {},
                dishes: []
            })
        }

        DishPlan.find({userId: req.userData.userId, planId: plan[0]._id}, async(err2, dishesInPlan) => {
            if (err2 || !dishesInPlan[0]) {
                console.error(err);
                res.status(500).json({
                    error: err
                })
                return;
            }
            try {

                let dishesFromPlan = [];

                for (i = 0; i < dishesInPlan.length; i++) {
                        const dishes = await Dish.find({userId: req.userData.userId, _id: dishesInPlan[i].productId})
                        const products = await Product.find({userId: req.userData.userId, _id: dishesInPlan[i].productId})
                        if (dishes[0]) {
                            dishesFromPlan.push({quantity: dishesInPlan[i].quantity, product: dishes[0]})
                        }
                        if (products[0]) {
                            dishesFromPlan.push({quantity: dishesInPlan[i].quantity, product: products[0]})
                        }
                        console.log(dishesFromPlan);
                }

                res.status(200).json({
                    plan: plan,
                    dishes: dishesFromPlan
                })

        } catch (error) {
            console.log(error);
        } 
        })
    })
})

router.get("/all", checkAuth, (req, res, next) => {
    Plan.find({userId: req.userData.userId}, (err, plans) => {
        if (err) console.error(err);

        res.status(200).json({
            plans: plans,
        })
    })
})

module.exports = router;
