const express = require("express");
const checkAuth = require("../middleware/check-auth");
const Product = require("../models/product");

const router = express.Router();

router.post("", checkAuth, (req, res, next) => {
    const product = new Product({
        userId: req.userData.userId,
        name: req.body.name,
        kcalAmount: req.body.kcalAmount,
        proteinAmount: req.body.proteinAmount,
        carbohydratesAmount: req.body.carbohydratesAmount
    })
    product.save()
        .then(result => {
            res.status(201).json({
                message: "Product created",
                result: result
            }) 
        }).catch (err => {
            res.status(500).json({
                error: err
            });
        });
})

router.get("", checkAuth, (req, res, next) => {
    Product.find({userId: req.userData.userId}, (err, products) => {
        console.log(products);

        if (err) console.error(err);

        res.status(200).json({
            products: products
        })
    })
})

module.exports = router;
