const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

const router = express.Router();

router.post("/signup", (req, res, next) => {
    bcrypt.hash(req.body.password, 12)
        .then(passwordHash => {
            const user = new User({
                email: req.body.email,
                password: passwordHash
            })
            user.save()
                .then(result => {
                    res.status(201).json({
                        message: "User created",
                        result: result
                    })
                }).catch (err => {
                    res.status(500).json({
                        error: err
                    });
                });
        })
})

router.post("/login", (req, res, next) => {
    let user2;

    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                return res.status(401).json({
                    message: "Auth failed"
                })
            }

            user2 = user;
            return bcrypt.compare(req.body.password, user.password);
        })
        .then( result => {
            if (!result) {
                return res.status(401).json({
                    message: "Auth failed"
                })
            }

            const token = jwt.sign({email: user2.email, userId: user2._id}, "To_jest_token_dzieki_ktoremu_user_bedzie_mial_dostep_do_odpowiednich_funkcjonalnosci",
            {expiresIn: "1h"});

            /// zwrocic id Usera
            res.status(200).json({
                token: token,
                userId: user2._id
            })
        })
        .catch(err => {
            return res.status(401).json({
                message: err
            })
        })
    
})

module.exports = router;
