import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { AuthData } from './auth-data';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _token: string;
  private _userId: string;
  private authStatusListener = new Subject<boolean>();
  private isAuthenticated = false;

  constructor(private http: HttpClient, private router: Router) { }

  get token(): string {
    return this._token;
  }
  set token(token: string) {
    this._token = token;
  }

  get userId(): string {
    return this._userId;
  }

  set userId(userId: string) {
    this._userId = userId;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  createUser(email: string, password: string): Promise<void> {
    return new Promise((resolve, reject) => {
      const authData: AuthData = { email: email, password: password };

      this.http.post('/api/user/signup', authData)
        .subscribe(
          res => resolve(),
          err => reject());
    })
  }

  login(email: string, password: string) {
    const authData: AuthData = { email: email, password: password };

    this.http.post<{ token: string, userId: string }>('/api/user/login', authData)
      .subscribe(response => {
        const token = response.token;
        this.userId = response.userId;
        this.token = token;

        if (token) {
          this.isAuthenticated = true;
          this.authStatusListener.next(true);
          this.router.navigate(['/summary']);
        }
      });
  }

  logout(): void {
    this.userId = ""
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.router.navigate(['/']);
  }
}
