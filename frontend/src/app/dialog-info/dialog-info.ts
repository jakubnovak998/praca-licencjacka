import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'dialog-info',
    templateUrl: 'dialog-info.html',
  })
  export class DialogInfo {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<DialogInfo>) {}
    exit(): void {
      this.dialogRef.close();
    }
  }