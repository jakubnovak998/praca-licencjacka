import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductsService } from '../products/products.service';
import { selectedDish } from './selectedDish';
import { Product } from '../products/product';

@Component({
  selector: 'dialog-product-list',
  templateUrl: 'dialog-product-list.html',
})
export class DialogProductList {
  dishes: Product[];
  quantitys: number[];
  addingsHidden: boolean[];
  selectedDishes: selectedDish[] = [];

  constructor(
    public productsService: ProductsService,
    public dialogRef: MatDialogRef<DialogProductList>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.getProductsAndDishes();
  }

  public getProductsAndDishes(): void {
    Promise.all([this.productsService.getProducts(), this.productsService.getDishes()]).then((result: any[]) => {
      const products = result[0];
      const dishes = result[1];

      this.dishes = [...products, ...dishes];
      this.quantitys = new Array(dishes.length);
      this.quantitys.forEach(element => {
        element = 1;
      });
      this.addingsHidden = new Array(dishes.length);
      this.addingsHidden.forEach(element => {
        element = false;
      });
    });
  }

  public selectDish(quantity: number, product: Product, id: number): void {
    let dish = { product: product, quantity: quantity };
    this.selectedDishes.push(dish);
    this.addingsHidden[id] = true;
  }

  displayedColumns: string[] = ['name', 'actions'];
}