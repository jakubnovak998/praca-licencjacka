import { Product } from '../products/product';


export interface selectedDish {
    product: Product;
    quantity: number
  }