import { TestBed } from '@angular/core/testing';

import { DietGoalService } from './diet-goal.service';

describe('DietGoalService', () => {
  let service: DietGoalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DietGoalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
