import { Injectable } from '@angular/core';
import { DietGoal } from './diet-goal';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DietGoalService {

  constructor(private http: HttpClient) { }

  saveDietGoal(value: number, unit: string, startDate: string): Promise<void> {
    return new Promise((resolve, reject) => {
      const dietGoal: DietGoal = { value: value, unit: unit, startDate: startDate };

      this.http.post('/api/diet-goal', dietGoal)
        .subscribe(
          res => resolve(),
          err => reject());
    })
  }

  updateDietGoal(value: number, unit: string, startDate: string): Promise<void> {
    return new Promise((resolve, reject) => {
      const dietGoal: DietGoal = { value: value, unit: unit, startDate: startDate };

      this.http.put('/api/diet-goal', dietGoal)
        .subscribe(
          res => resolve(),
          err => reject());
    });
  }

  getDietGoal() {
    return new Promise((resolve, reject) => {
      this.http.get('/api/diet-goal')
        .subscribe(response => {
          console.log(response);
          resolve(response);
        });
    })
  }
}
