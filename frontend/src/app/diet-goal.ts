export interface DietGoal {
    value: number;
    unit: string;
    startDate: string
}