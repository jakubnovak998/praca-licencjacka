import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { DialogProductList } from '../dialog-product-list/dialog-product-list';
import { NewPlan, Plan } from './plan';
import { PlanService } from './plan.service';
import { selectedDish } from '../dialog-product-list/selectedDish';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {

  scheduleDay: Date;
  dateExtra: string;
  dishes: selectedDish[];
  dayKcalAmount: number = 0;
  dayCarbohydratesAmount: number = 0;
  dayproteinAmount: number = 0;
  plan: Plan;

  constructor(public datepipe: DatePipe, public dialog: MatDialog, public planService: PlanService) { }

  ngOnInit(): void {
    this.scheduleDay = new Date();
    this.dateExtra =this.datepipe.transform(this.scheduleDay, 'dd/MM/yyyy');
    this.getPlanData();
  }

  getPlanData(): void {
    this.planService.getPlan(this.transformDate(this.dateExtra)).then((result: any) => {
      if (!result.plan || !result.dishes) {
        return;
      }
      this.plan = result.plan;
      this.dishes = result.dishes;
    });
  }

  transformDate(dateString) {
    let d = dateString.split("/");
    return  d[1] + "/" + d[0] + "/" + d[2];
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogProductList, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.dishes = result;
    });
  }

  addDay(): void {
    this.changeScheduleDay(1);
    this.getPlanData();
  }

  subtractDay(): void {
    this.changeScheduleDay(-1);
    this.getPlanData();
  }

  changeScheduleDay(numberOfDays: number): void {
    this.scheduleDay = new Date ( 
    this.scheduleDay.getFullYear(),
    this.scheduleDay.getMonth(),
    this.scheduleDay.getDate() + numberOfDays );
    this.dateExtra =this.datepipe.transform(this.scheduleDay, 'dd/MM/yyyy');
  }

  savePlan(): void {
    const planDay = this.datepipe.transform(this.scheduleDay, 'MM/dd/yyyy');

    this.dishes.forEach(dish => {
      this.dayKcalAmount += dish.quantity * dish.product.kcalAmount;
      this.dayCarbohydratesAmount += dish.quantity * dish.product.carbohydratesAmount;
      this.dayproteinAmount += dish.quantity * dish.product.proteinAmount;
    });
    const plan: NewPlan = {date: planDay, kcalAmount: this.dayKcalAmount, carbohydratesAmount: this.dayCarbohydratesAmount,
      proteinAmount: this.dayproteinAmount, dishes: this.dishes};
    this.planService.savePlan(plan);
  }

}
