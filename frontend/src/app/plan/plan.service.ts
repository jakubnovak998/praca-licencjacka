import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NewPlan } from './plan';

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor(private http: HttpClient) { }

  savePlan(plan: NewPlan) {
    this.http.post('/api/plan', plan)
      .subscribe(response => {
        console.log(response);
      });
  }

  getPlan(date: string) {
    return new Promise((resolve, reject) => {
      let params = new HttpParams();
      params = params.append('date', date);
      this.http.get('/api/plan',  {params: params})
      .subscribe((result: any) => {
        resolve(result);
    });
  })
  }

  getAllPlans(date: string) {
    return new Promise((resolve, reject) => {
      let startDate = new Date(date).getTime();

      this.http.get('/api/plan/all')
      .subscribe((result: any) => {
          let filteredPlans = result.plans.filter(function(plan) {
          let planDate = new Date(plan.date).getTime();
          return planDate >= startDate;
        });

        resolve(filteredPlans);
    });
  })
  }
}
