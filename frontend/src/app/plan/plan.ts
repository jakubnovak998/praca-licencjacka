

export interface Plan {
    _id: string,
    date: string,
    name: string,
    kcalAmount: number,
    proteinAmount: number,
    carbohydratesAmount: number,
}

export interface NewPlan {
    date: string,
    kcalAmount: number,
    proteinAmount: number,
    carbohydratesAmount: number,
    dishes: any
}