export interface Dish {
    name: string,
    ingredients: ingredient[],
    kcalAmount: number,
    proteinAmount: number,
    carbohydratesAmount: number
}

interface ingredient {
    quantity: number, productId: string
}