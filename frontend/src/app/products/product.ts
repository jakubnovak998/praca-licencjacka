export interface Product {
    _id: string
    name: string,
    kcalAmount: number,
    proteinAmount: number,
    carbohydratesAmount: number,
}

export interface NewProduct {
    name: string,
    kcalAmount: number,
    proteinAmount: number,
    carbohydratesAmount: number,
}