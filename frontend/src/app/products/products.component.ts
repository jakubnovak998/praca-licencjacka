import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ProductsService } from './products.service';
import { NewProduct, Product } from './product';
import { Dish } from './dish';
import { DialogInfo } from '../dialog-info/dialog-info';
import { MatDialog } from '@angular/material/dialog';

export interface PeriodicElement {
  name: string;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})

export class ProductsComponent implements OnInit {

  products: Product[];
  ingredients: { quantity: number, productId: string }[] = [];
  quantitys: number[];
  addingsHidden: boolean[];
  dishKcalAmount = 0;
  dishProteinAmount = 0;
  dishCarbohydratesAmount = 0;

  constructor(public productsService: ProductsService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getProducts();
  }

  public getProducts(): void {
    this.productsService.getProducts().then((products: Product[]) => {
      this.products = [...products];
      this.quantitys = new Array(products.length);
      this.quantitys.forEach(element => {
        element = 1;
      });
      this.addingsHidden = new Array(products.length);
      this.addingsHidden.forEach(element => {
        element = false;
      });
    })
  }

  openDialog(text: String) {
    const dialogRef = this.dialog.open(DialogInfo, {
      width: '450px',
      data: {
        text: text
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  productForm: FormGroup = new FormGroup({
    name: new FormControl(''),
    kcalAmount: new FormControl(''),
    proteinAmount: new FormControl(''),
    carbohydratesAmount: new FormControl(''),
  });

  dishForm: FormGroup = new FormGroup({
    name: new FormControl(''),
  });

  public submitProduct() {
    if (this.productForm.valid) {
      const product: NewProduct = { name: this.productForm.value.name, kcalAmount: this.productForm.value.kcalAmount, proteinAmount: this.productForm.value.proteinAmount, carbohydratesAmount: this.productForm.value.carbohydratesAmount };
      this.productsService.saveProduct(product).then(() => {
        this.openDialog("Dodano produkt");
      }).catch(() => {
        this.openDialog("Blad przy dodawaniu produktu");
      });
      this.getProducts();
    }
  }

  public submitDish() {
    if (this.dishForm.valid) {
      const dish: Dish = {
        name: this.dishForm.value.name, ingredients: this.ingredients, kcalAmount: this.dishKcalAmount, carbohydratesAmount: this.dishCarbohydratesAmount,
        proteinAmount: this.dishProteinAmount
      };
      this.productsService.saveDish(dish).then(() => {
        this.openDialog("Dodano posiłek");
      }).catch(() => {
        this.openDialog("Blad przy dodawaniu dania");
      });
    }
  }

  private addIngredient(quantity: number, product: Product, id: number): void {
    let ingredient = { quantity: quantity, productId: product._id };
    this.ingredients.push(ingredient);
    this.addingsHidden[id] = true;
    this.dishKcalAmount += quantity * product.kcalAmount;
    this.dishProteinAmount += quantity * product.proteinAmount;
    this.dishCarbohydratesAmount += quantity * product.carbohydratesAmount;
  }

  displayedColumns: string[] = ['name', 'actions'];
}
