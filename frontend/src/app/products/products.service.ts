import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NewProduct, Product } from './product';
import { Dish } from './dish';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private productsSubject = new BehaviorSubject([]);
  observable$: Observable<Array<Product>> = this.productsSubject.asObservable();

  constructor(private http: HttpClient) { }

  saveProduct(product: NewProduct): Promise<void> {
    return new Promise((resolve, reject) => {
    this.http.post('/api/product', product)
      .subscribe(  
        res => resolve(),
        err => reject());
    })
  }

  getProducts() {
    return new Promise((resolve, reject) => {
      this.http.get('/api/product')
      .subscribe((result: any) => {
      let products = result.products;
      resolve(products);
    });
  })
  }

  saveDish(dish: Dish): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/dish', dish)
        .subscribe(response => {
          resolve();
        });
    })
  }

  getDishes() {
    return new Promise((resolve, reject) => {
      this.http.get('/api/dish')
      .subscribe((result: any) => {
      let dishes = result.dishes;
      resolve(dishes);
    });
  })
  }
}
