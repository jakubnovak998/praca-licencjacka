import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DietGoalService } from '../diet-goal.service';
import { AuthService } from '../auth.service';
import { multi } from './data';
import { PlanService } from '../plan/plan.service';
import { Plan } from '../plan/plan';
import { DietGoal } from '../diet-goal';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { DialogInfo } from '../dialog-info/dialog-info';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  userId: string;

  multi: any[];
  view: any[] = [700, 300];
  editingDietGoalEnabled: boolean = false;

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Data';
  yAxisLabel: string = 'Realizacja';
  timeline: boolean = true;
  plans: Plan[];
  dietGoal: DietGoal;
  data: any[];
  showStats: boolean = false;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  constructor(public dietGoalSerivce: DietGoalService, public authService: AuthService, public planService: PlanService, public dialog: MatDialog, public datepipe: DatePipe) {
    Object.assign(this, { multi });
  }

  ngOnInit(): void {
    this.userId = this.authService.userId;
    this.getData();
  }

  getData() {
    this.dietGoalSerivce.getDietGoal().then((dietGoalResult: any) => {
      this.dietGoal = dietGoalResult.dietGoal[0];

      this.planService.getAllPlans(this.dietGoal.startDate).then((plans: any) => {
        this.plans = plans;
        if (this.plans) {
          this.createChartData();
        }
      })
    });
  }

  dietGoalForm: FormGroup = new FormGroup({
    value: new FormControl(1),
    unit: new FormControl(''),
  });

  disableDietGoalSave(): boolean {
    if (!this.dietGoal) {
      return true;
    }
    return false;
  }

  editGoal(): void {
    this.editingDietGoalEnabled = true;
    this.dietGoalForm = new FormGroup({
      value: new FormControl(this.dietGoal.value),
      unit: new FormControl(this.dietGoal.unit),
    });
  }

  createChartData(): void {
    let goalSeries = [];
    let realisationSeries = [];
    let unit = this.dietGoal.unit;
    let realisationText;

    this.plans.forEach(plan => {
      let value;

      if (unit === 'kcal') {
        value = plan.kcalAmount;
      } else if (unit === 'protein') {
        value = plan.proteinAmount;
      } else {
        value = plan.carbohydratesAmount;
      }
      realisationSeries.push({ name: this.transformDateToChart(plan.date), value: value })
    });

    for (let i = 0; i < realisationSeries.length; i++) {
      goalSeries[i] = { name: realisationSeries[i].name, value: this.dietGoal.value }
    }

    if (unit === 'kcal') {
      realisationText = "Realizacja (kcal)";
    } else if (unit === 'protein') {
      realisationText = "Realizacja (bialko)";
    } else {
      realisationText = "Realizacja (weglowodany)";
    }

    this.multi[0].series = goalSeries;
    this.multi[1].series = realisationSeries;
    this.multi[1].name = realisationText;
    this.yAxisLabel = realisationText;
    this.data = Object.assign(this.multi);
    
    if (realisationSeries.length !== 0) {
      this.showStats = true;
    }
  }

  getUnit() {
    if (this.dietGoal.unit === "kcal") {
      return "";
    } else {
      return "g";
    }
  }

  translateUnit() {
    if (this.dietGoal.unit === "protein") {
      return "białka";
    } else if (this.dietGoal.unit === "kcal") {
      return "kcal";
    } else {
      return "weglowodanow";
    }
  }

  transformDateToChart(dateString) {
    let d = dateString.split("/");
    return  d[1] + "/" + d[0] + "/" + d[2];
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  openDialog(text: String) {
    const dialogRef = this.dialog.open(DialogInfo, {
      width: '450px',
      data: {
        text: text
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  saveDietGoal(): void {
    const currentDate = new Date().getTime();
    const trasformedCurrentDate = this.datepipe.transform(currentDate, 'MM/dd/yyyy');

    if (this.dietGoalForm.valid) {
      if (this.editingDietGoalEnabled === true) {
        this.dietGoalSerivce.updateDietGoal(this.dietGoalForm.value.value, this.dietGoalForm.value.unit, trasformedCurrentDate).then(() => {
          this.openDialog("Zaktualizowano cel dietetyczny");
          this.getData();
        }).catch(() => {
          this.openDialog("Blad przy aktualizacji celu dietetycznego");
        });
      } else {
        this.dietGoalSerivce.saveDietGoal(this.dietGoalForm.value.value, this.dietGoalForm.value.unit, trasformedCurrentDate).then(() => {
          this.openDialog("Utworzono cel dietetyczny");
          this.getData();
        }).catch(() => {
          this.openDialog("Blad przy tworzeniu celu dietetycznego");
        });
      }
    }
  }
}
